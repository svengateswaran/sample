# -*- coding: utf-8 -*-
#
# -- General configuration -----------------------------------------------------

source_suffix = '.txt'

#Name of the master file 
master_doc = 'index'

# General information about the project.
#This is the title of the project which appears on the top of HTML Document as well as PDF Document
project = u'Sample'


#This is the Copyright Information that will appear on the bottom of the document
copyright = u'2014 MulticoreWare Inc - Vengateswaran'


# -- Options for HTML output ---------------------------------------------------

html_theme = "default"
